#include <iostream>
#include <vector>
#include <SDL.h>

#include "GameMain.h"

// SDL projects do not automatically work with the console window.
// On windows with visual studio, the following line is required to use console output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

using namespace std;

// Program entry point
// SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[])
{
	GameMain *game = GameMain::getInstance();
	game->init();
	game->run();
	GameMain::deleteInstance();
    return 0;
}