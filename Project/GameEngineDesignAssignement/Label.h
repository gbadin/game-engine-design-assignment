//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : Label.h
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#if !defined(_LABEL_H)
#define _LABEL_H

#include "RenderNode.h"
#include "Renderer.h"
#include <string>

#include "RenderNodeFlags.h"

class Label : public RenderNode
{
public:
	Label(){m_colour = new int[3];m_modified = true;}
	Label(float x, float y, float dimension, std::string text, int colour[3], int params = DIMENSION_HEIGHT);
	~Label(){}

	float getX(){return m_x;}
	void setX(float x){m_x = x;}
	float getY(){return m_y;}
	void setY(float y){m_y = y;}
	void setDimension(float dimension){m_dimension = dimension;}
	float getDimension(){return m_dimension;}
	void changeText(std::string text);
	std::string getText(){return m_text;}
	int* getColour(){return m_colour;}
	void setColour(int colour[3]);
	void setFlags(int param){m_flags = param;}
	//getters for the flag
	bool isXCentered(){return (m_flags & X_CENTERED)!= 0;}
	bool isDimensionIsHeight(){return (m_flags & DIMENSION_WIDTH) == 0;}//we check if the dimension is not the width because m_flags & DIMENSION_HEIGHT is always false
	bool isDimensionIsWidth(){return (m_flags & DIMENSION_WIDTH) != 0;}
	bool isYCentered(){return (m_flags & Y_CENTERED)!= 0;}

	void render(Renderer* visitor);
private:
	//int colour[3]
	int *m_colour;
	std::string m_text;
	unsigned int m_TexID;
	bool m_modified;
	// width or height of the label (depending on the flag DIMENSION_HEIGHT and DIMENSION_WIDTH)
	float m_dimension;
	float m_x;
	float m_y;
	int m_flags;
};

#endif  //_LABEL_H
