//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : SimpleAttack.h
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#if !defined(_SIMPLEATTACK_H)
#define _SIMPLEATTACK_H

#include "AbstractAttackBehaviour.h"

class SimpleAttack : public AbstractAttackBehaviour
{
public:
	int attack(AbstractCharacter *attacker, AbstractCharacter *attacked);
};

#endif  //_SIMPLEATTACK_H
