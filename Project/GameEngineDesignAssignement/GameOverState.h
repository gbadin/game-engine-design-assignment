//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : GameOverState.h
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#if !defined(_GAMEOVERSTATE_H)
#define _GAMEOVERSTATE_H

#include "GameState.h"
#include "Renderer.h"
#include "Label.h"
#include <string>

class GameOverState : public GameState
{
public:
	GameOverState();
	~GameOverState(){}
	void render(Renderer *visitor);
	std::string nextState();
	bool isStateEnded();
	void update();
	void OnEntry();
	void updtateState(float timeElapsed);
	void OnExit();
private:
};

#endif  //_GAMEOVERSTATE_H
