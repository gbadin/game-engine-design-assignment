//
//
//  @ Project : Galactic Marine
//  @ File Name : EventsHandler.h
//  @ Date : 12/9/2012
//  @ Author : Gr�goire Badin
//
//
#if !defined(_EVENTSHANDLER_H)
#define _EVENTSHANDLER_H

#include "KeyboardInput.h"

class EventsHandler
{
public:
	EventsHandler(){m_keyBoard = NULL;}
	EventsHandler(KeyboardInput *keyBoard):m_keyBoard(keyBoard){}
	virtual ~EventsHandler(){}
	void setKeyBoard(KeyboardInput *keyBoard){if(keyBoard != NULL)m_keyBoard = keyBoard;}
	KeyboardInput *getKeyBoard(){return m_keyBoard;}
	virtual bool handleEvent() = 0;
protected:
	KeyboardInput *m_keyBoard;
};

#endif