//
//  @ Project : Galactic Marine
//  @ File Name : FileReader.h
//  @ Date : 12/10/2012
//  @ Author : Gr�goire Badin
//
//

#include <string>
#include <sstream>

class FileReaderWriter
{
public:
	//can throw GameEngineException
	static std::stringstream Readfile(std::string fileName);
	static void Writefile(std::string fileName, std::string *text);
};

