//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : KeyboardMovement.cpp
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#include "KeyboardMovement.h"
#include "GameMain.h"

#include <cmath>
#include <SDL_keycode.h>

using namespace std;

KeyboardMovement::KeyboardMovement()
{
	m_subject = GameMain::getInstance()->getKeyBoardInput();
	m_currentDirection = NO_DIRECTION;
	m_directionLocked = NO_DIRECTION;
}

void KeyboardMovement::move(AbstractCharacter* mover,float timeElapsed)
{
	if(timeElapsed < 100)
	{
		float xmove, ymove, ratioXAxis, ratioYAxis;
		ratioXAxis = 0;
		ratioYAxis = 0;
		if((m_currentDirection & (DIRECTION_UP | DIRECTION_DOWN)) != (DIRECTION_UP | DIRECTION_DOWN))//check if there is not two key (up and down) pressed at the same time.
		{
			if((m_currentDirection & DIRECTION_UP) != 0 && (m_directionLocked & DIRECTION_UP) == 0)
			{
				ratioYAxis = 1.0f;
				unlockDirection(DIRECTION_DOWN);
			}
			else if((m_currentDirection & DIRECTION_DOWN) != 0 && (m_directionLocked & DIRECTION_DOWN) == 0)
			{
				ratioYAxis = -1.0f;
				unlockDirection(DIRECTION_UP);
			}
		}

		if((m_currentDirection & (DIRECTION_LEFT | DIRECTION_RIGHT)) != (DIRECTION_LEFT | DIRECTION_RIGHT))//check if there is not two key (up and down) pressed at the same time.
		{
			if((m_currentDirection & DIRECTION_RIGHT) != 0 && (m_directionLocked & DIRECTION_RIGHT) == 0)
			{
				ratioXAxis = 1.0f;
				unlockDirection(DIRECTION_LEFT);
			}
			else if((m_currentDirection & DIRECTION_LEFT) != 0 && (m_directionLocked & DIRECTION_LEFT) == 0)
			{
				ratioXAxis = -1.0f;
				unlockDirection(DIRECTION_RIGHT);
			}
		}

		//check for diagonal movement
		if(ratioYAxis != 0 && ratioXAxis != 0)
		{
			ratioYAxis = ratioYAxis/sqrt(2.0f);
			ratioXAxis = ratioXAxis/sqrt(2.0f);
		}

		//calculate the move amounth
		xmove = ratioXAxis*timeElapsed*(MOVE_PER_SEC/1000.0f);
		ymove = ratioYAxis*timeElapsed*(MOVE_PER_SEC/1000.0f);
		mover->setX(mover->getX()+xmove);
		mover->setY(mover->getY()+ymove);
	}
}

void KeyboardMovement::update()
{
	set<int> keys = m_subject->getKeyDown();
	set<int>::iterator it;
	for(it = keys.begin(); it != keys.end(); it++)
	{
		//for each key down, we add them to the direction
		switch(*it)
		{
			//to be sure that there will be no problem, the bitwise operator OR is used to prevent the case were a key is pressed,
			//not released in the program, and when we go again in the program, the key is pressed again.
			case SDLK_UP: case SDLK_w:
				m_currentDirection = m_currentDirection | DIRECTION_UP;
				break;
			case SDLK_DOWN: case SDLK_s:
				m_currentDirection = m_currentDirection | DIRECTION_DOWN;
				break;
			case SDLK_RIGHT: case SDLK_d:
				m_currentDirection = m_currentDirection | DIRECTION_RIGHT;
				break;
			case SDLK_LEFT: case SDLK_a:
				m_currentDirection = m_currentDirection | DIRECTION_LEFT;
				break;
			default:
				break;
		}
	}
	keys = m_subject->getKeyUp();
	for(it = keys.begin(); it != keys.end(); it++)
	{
		//for each key down, we add them to the direction
		switch(*it)
		{
			case SDLK_UP: case SDLK_w:
				//same thing that for the key up, but this time, it's to be sure that we remove a direction of a key that is really pressed.
				// can be replaced by a NOR bitwise operator if exists
				m_currentDirection = m_currentDirection | DIRECTION_UP;
				m_currentDirection -= DIRECTION_UP;
				break;
			case SDLK_DOWN: case SDLK_s:
				m_currentDirection = m_currentDirection | DIRECTION_DOWN;
				m_currentDirection -= DIRECTION_DOWN;
				break;
			case SDLK_RIGHT: case SDLK_d:
				m_currentDirection = m_currentDirection | DIRECTION_RIGHT;
				m_currentDirection -= DIRECTION_RIGHT;
				break;
			case SDLK_LEFT: case SDLK_a:
				m_currentDirection = m_currentDirection | DIRECTION_LEFT;
				m_currentDirection -= DIRECTION_LEFT;
				break;
			default:
				break;
		}
	}
}

