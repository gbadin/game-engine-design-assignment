//
//
//  @ Project : Galactic Marine
//  @ File Name : SDLEventHandler.h
//  @ Date : 12/9/2012
//  @ Author : Gr�goire Badin
//
//
#if !defined(_SDLEVENTSHANDLER_H)
#define _SDLEVENTSHANDLER_H

#include "eventshandler.h"
#include <map>

class SDLEventHandler :public EventsHandler
{
public:
	SDLEventHandler():EventsHandler(){}
	SDLEventHandler(KeyboardInput *keyBoard);
	~SDLEventHandler(){delete m_keyBoard;}
	bool handleEvent();
private:
	std::map<int,int> m_keyCodes;
};

#endif