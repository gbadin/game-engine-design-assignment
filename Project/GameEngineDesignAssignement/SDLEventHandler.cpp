//
//
//  @ Project : Galactic Marine
//  @ File Name : SDLEventHandler.cpp
//  @ Date : 12/9/2012
//  @ Author : Gr�goire Badin
//
//

#include "SDLEventHandler.h"
#include "KeyboardInput.h"
#include <SDL.h>
#include <map>

using namespace std;

SDLEventHandler::SDLEventHandler(KeyboardInput *keyBoard):EventsHandler(keyBoard)
{
	m_keyCodes = map<int,int>();
}

bool SDLEventHandler::handleEvent()
{
	SDL_Event sdlEvent;
	set<int> keyUp,keyDown;
	keyUp = set<int>();
	keyDown = set<int>();
	while(SDL_PollEvent(&sdlEvent))
	{
		if(sdlEvent.type == SDL_QUIT)
		{
			return false;
		}
		else if (sdlEvent.type == SDL_KEYUP && m_keyBoard!=NULL && sdlEvent.key.repeat == 0)
		{
			keyUp.insert(sdlEvent.key.keysym.sym);
		}
		else if(sdlEvent.type == SDL_KEYDOWN && m_keyBoard!=NULL && sdlEvent.key.repeat == 0)
		{
			keyDown.insert(sdlEvent.key.keysym.sym);
		}
	}
	if(!keyUp.empty() || !keyDown.empty())
		m_keyBoard->handleEvent(keyUp, keyDown);
	return true;
}