//
//  @ Project : Galactic Marine
//  @ File Name : RenderNodeFlags.h
//  @ Date : 12/10/2012
//  @ Author : Gr�goire Badin
//
//

#ifndef RENDER_NODE_FLAGS_H
#define RENDER_NODE_FLAGS_H

	#define NOT_CENTERED 0 //0b000
	#define X_CENTERED 1 //0b001
	#define Y_CENTERED 2 //0b010
	#define ALL_CENTERED 3 //0b011
	//by default, the dimension is the height so the flag is at 0b000 and 0b100 for the width
	#define DIMENSION_HEIGHT 0 //0b000
	#define DIMENSION_WIDTH 4 //0b100

#endif