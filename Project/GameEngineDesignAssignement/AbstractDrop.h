//
//
//  @ Project : Galactic Marine
//  @ File Name : Drops.h
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#ifndef ABSTRACT_DROPS_H
#define ABSTRACT_DROPS_H

class AbstractDrop
{
public:
	AbstractDrop(){}
	virtual ~AbstractDrop(){}
	virtual int getMoneyDroped() = 0;
	virtual void initItemsToDrop() =0;
	virtual int getNumberItemDropped() =0;
	virtual int *getItemsDroped() = 0;
	virtual AbstractDrop* getCopy() =0;
};

#endif
