//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : RenderNode.h
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#if !defined(_RENDERNODE_H)
#define _RENDERNODE_H

#include "Renderer.h"

class RenderNode
{
public:
	virtual void render(Renderer* visitor) = 0;
	virtual ~RenderNode(){}
};

#endif  //_RENDERNODE_H
