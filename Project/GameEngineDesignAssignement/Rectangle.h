//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : Rectangle.h
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#if !defined(_RECTANGLE_H)
#define _RECTANGLE_H

#include "RenderNode.h"
#include "Renderer.h"
#include "RenderNodeFlags.h"

class Rectangle : public RenderNode
{
public:
	Rectangle(){m_colour = new int[3];}
	Rectangle(float x, float y, float width, float height, int colour[3], int params = NOT_CENTERED);
	~Rectangle(){}

	float getX(){return m_x;}
	float getY(){return m_y;}
	float getHeight(){return m_height;}
	float getWidth(){return m_width;}
	void setX(float x){m_x = x;}
	void setY(float y){m_y = y;}
	void setHeight(float height){m_height = height;}
	void setWidth(float width){m_width = width;}
	int* getColour(){return m_colour;}
	void setColour(int colour[3]);
	void setFlags(int param){m_flags = param;}
	//getters for the flag
	bool isXCentered(){return (m_flags & X_CENTERED) != 0;}
	bool isYCentered(){return (m_flags & Y_CENTERED) != 0;}

	void render(Renderer* visitor);
private :
	//int m_color[3]
	int *m_colour;
	float m_height;
	float m_width;
	float m_x;
	float m_y;
	int m_flags;
};

#endif  //_RECTANGLE_H
