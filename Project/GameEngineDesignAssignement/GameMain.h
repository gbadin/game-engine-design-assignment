//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : GameMain.h
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#if !defined(_GAMEMAIN_H)
#define _GAMEMAIN_H

#include "PlayerFactory.h"
#include "MonsterFactory.h"
#include "ItemFactory.h"
#include "StatFactory.h"
#include "ItemPropertyFactory.h"

#include "Saveable.h"
#include "StateList.h"
#include "Renderer.h"
#include "KeyboardInput.h"
#include "EventsHandler.h"
#include <string>

class GameMain : public Saveable
{
public:
	static GameMain* getInstance();
	static void deleteInstance();

	void run();
	void init();

	StateList *getStateList();
	std::string getCurrentState(){return m_currentState;}

	Renderer* getRenderer(){return m_renderer;}
	void setRenderer(Renderer* renderer){if(renderer != NULL)m_renderer = renderer;}
	EventsHandler* getEventHandler(){return m_events;}
	void setEventHandler(EventsHandler* events){if(events != NULL)m_events = events;}

	float getTimeElapsed(){return m_timeElapsed;}

	PlayerFactory *getPlayerFactory(){return m_pFactory;}
	ItemFactory *getItemFactory(){return m_itemFactory;}
	MonsterFactory *getMonsterFactory(){return m_mFactory;}
	StatFactory *getStatFactory(){return m_statFactory;}
	ItemPropertyFactory *getItemPropertyFactory(){return m_ItemPropertyFactory;}
	KeyboardInput *getKeyBoardInput(){return m_keyboardInput;}

	bool continueEnabled;

	void loadSave();
	std::string saveText();
	void loadInformationFromText(std::string text);
private:
	GameMain();
	~GameMain();

	Renderer* m_renderer;
	EventsHandler *m_events;

	std::string m_currentState;
	StateList m_states;

	float m_timeElapsed;

	PlayerFactory *m_pFactory;
	MonsterFactory *m_mFactory;
	ItemFactory *m_itemFactory;
	StatFactory *m_statFactory;
	ItemPropertyFactory *m_ItemPropertyFactory;

	KeyboardInput *m_keyboardInput;

	static GameMain *m_instance;
};

#endif  //_GAMEMAIN_H
