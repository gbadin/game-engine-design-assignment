//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : PlayingMap.h
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#if !defined(_PLAYINGMAP_H)
#define _PLAYINGMAP_H

#include "Saveable.h"
#include "InteractionPossible.h"
#include "Player.h"
#include <set>
#include <string>

class PlayingMap : public Saveable
{
public:
	PlayingMap(){}
	PlayingMap(std::set<InteractionPossible*> NPCAndObjects, Player* player,float width = 2,float height = 2);
	~PlayingMap();

	void addInteractionPossible(InteractionPossible *object);
	void removeInteractionPossible(InteractionPossible *object);
	void setPlayer(Player *player);
	Player *getPlayer(){return m_player;}
	PlayingMap &operator=(PlayingMap &otherMap);
	void setWidth(float width){if(width > 0)m_width =width;}
	float getWidth(){return m_width;}
	void setHeight(float height){if(height > 0)m_height =height;}
	float getHeight(){return m_height;}
	std::set<InteractionPossible*> *getInteractionPossibles(){return &m_NPCAndObjects;}

	std::string saveText();
	void loadInformationFromText(std::string text);
protected:
	std::set<InteractionPossible*> m_NPCAndObjects;
	Player* m_player;
	float m_width;
	float m_height;
};

#endif  //_PLAYINGMAP_H
