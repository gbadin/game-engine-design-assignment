//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Galactic Marine
//  @ File Name : Player.cpp
//  @ Date : 12/4/2012
//  @ Author : Gr�goire Badin
//
//


#include "Player.h"
#include "RenderNode.h"
#include "Rectangle.h"
#include "Label.h"
#include <vector>
#include <map>
#include <string>

using namespace std;

InteractionPossible* Player::returnCopy()
{
	Player *playerToReturn = new Player();
	*playerToReturn = *this;
	return playerToReturn;
}

Player &Player::operator=(Player &otherPlayer)
{
	m_width = otherPlayer.getWidth();
	m_height = otherPlayer.getHeight();
	m_x = otherPlayer.getX();
	m_y = otherPlayer.getY();

	m_stats = otherPlayer.getStats();

	m_ID = otherPlayer.getID();
	m_name = otherPlayer.getName();

	m_needMoreAccurateCollision = otherPlayer.m_needMoreAccurateCollision;

	setColour(otherPlayer.getColour());

	m_renderers.clear();

	*m_moveBehaviour = *otherPlayer.getMoveBehaviour();
	*m_attackBehaviour = *otherPlayer.getAttackBehaviour();

	return *this;
}

void Player::updateRenderers()
{
	// if there is no renderers, we create them
	if(m_renderers.size() == 0)
	{
		Rectangle *tmprect = new Rectangle(m_x,m_y,m_width,m_height,m_colour);
		m_renderers.push_back(tmprect);
		Label *tmpLabel = new Label(m_x +m_width/2,m_y+m_height,m_width,"Player",m_colour,DIMENSION_WIDTH | X_CENTERED);
		m_renderers.push_back(tmpLabel);
	}
	//else, we change them
	else
	{
		Rectangle *tmprect = dynamic_cast<Rectangle*>(m_renderers[0]);
		//check if the rectangle really exists
		if(tmprect != NULL)
		{
			tmprect->setColour(m_colour);
			tmprect->setX(m_x);
			tmprect->setY(m_y);
			tmprect->setWidth(m_width);
			tmprect->setHeight(m_height);
		}
		else
			tmprect = new Rectangle(m_x,m_y,m_width,m_height,m_colour);

		Label *tmpLabel = dynamic_cast<Label*>(m_renderers[1]);
		//check if the label really exists
		if(tmpLabel != NULL)
		{
			tmpLabel->setColour(m_colour);
			tmpLabel->setX(m_x +m_width/2);
			tmpLabel->setY(m_y+m_height);
			tmpLabel->setDimension(m_width);
		}
		else
			tmpLabel = new Label(m_x +m_width/2,m_y+m_height,m_width,"Player",m_colour,DIMENSION_WIDTH | X_CENTERED);
	}
}

vector<RenderNode*> Player::getRenderNodeToPosition(float x, float y)
{
	// if there is no renderers, we create them
	if(m_renderers.size() == 0)
	{
		Rectangle *tmprect = new Rectangle(x,y,m_width,m_height,m_colour);
		m_renderers.push_back(tmprect);
		Label *tmpLabel = new Label(x +m_width/2,y+m_height,m_width,"Player",m_colour,DIMENSION_WIDTH | X_CENTERED);
		m_renderers.push_back(tmpLabel);
	}
	//else, we change them
	else
	{
		Rectangle *tmprect = dynamic_cast<Rectangle*>(m_renderers[0]);
		//check if the rectangle really exists
		if(tmprect != NULL)
		{
			tmprect->setColour(m_colour);
			tmprect->setX(x);
			tmprect->setY(y);
			tmprect->setWidth(m_width);
			tmprect->setHeight(m_height);
		}
		else
			tmprect = new Rectangle(x,y,m_width,m_height,m_colour);

		Label *tmpLabel = dynamic_cast<Label*>(m_renderers[1]);
		//check if the label really exists
		if(tmpLabel != NULL)
		{
			tmpLabel->setColour(m_colour);
			tmpLabel->setX(x +m_width/2);
			tmpLabel->setY(y+m_height);
			tmpLabel->setDimension(m_width);
		}
		else
			tmpLabel = new Label(x +m_width/2,y+m_height,m_width,"Player",m_colour,DIMENSION_WIDTH | X_CENTERED);
	}
	return m_renderers;
}

string Player::saveText()
{
	return "";
}

void Player::loadInformationFromText(string text)
{

}

bool Player::moreAccurateCollision(InteractionPossible &otherOject)
{
	return true;
}