//
//  @ Project : Galactic Marine
//  @ File Name : FileReader.cpp
//  @ Date : 12/10/2012
//  @ Author : Gr�goire Badin
//
//

#include "FileReaderWriter.h"
#include "GameEngineException.h"

#include <string>
#include <fstream>
#include <sstream>

using namespace std;

stringstream FileReaderWriter::Readfile(string fileName)
{
	ifstream file(fileName);
	if(!file.is_open())
	{
		stringstream strError;
		strError << "Error while opening file : " << fileName;
		throw new GameEngineException(ERR_FILE_NOT_OPENED,"class FileReaderWriter : static method : string Readfile(string fileName)",strError.str());
	}
	stringstream OutputText;
	string line;
	while(getline(file,line))
	{
		OutputText << line << endl;
	}
	file.close();
	OutputText.seekg(ios::beg);
	return OutputText;
}

void FileReaderWriter::Writefile(string fileName, string *text)
{
	ofstream file(fileName);
	if(!file.is_open())
	{
		stringstream strError;
		strError << "Error while opening file : " << fileName;
		throw new GameEngineException(ERR_FILE_NOT_OPENED,"class FileReaderWriter : static method : string Writefile(string fileName, string *text)",strError.str());
	}
	file << *text;
	file.close();
}